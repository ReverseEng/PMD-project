Warning
=======

This was a fork of [PMD](https://pmd.github.io) for experimental performance enhancements.
Please, refer to the official repository.

# PMD

## About

**PMD** is a source code analyzer. It finds common programming flaws like unused variables, empty catch blocks,
unnecessary object creation, and so forth. It supports Java, JavaScript, Salesforce.com Apex, PLSQL, Apache Velocity,
XML, XSL.

Additionally it includes **CPD**, the copy-paste-detector. CPD finds duplicated code in
Java, C, C++, C#, Groovy, PHP, Ruby, Fortran, JavaScript, PLSQL, Apache Velocity, Scala, Objective C,
Salesforce.com Apex, Perl, Swift, Matlab, Python.

## Source and Documentation

The latest source of PMD can be found on [GitHub](https://github.com/pmd/pmd).

## News and Website

More information can be found on PMD's [website](https://pmd.github.io) and on [SourceForge](https://sourceforge.net/projects/pmd/).
